<?php


$preferredSonosRoomsExists = false;
$preferredSonosRooms= json_decode(getConfigValue('sonos_preferred_rooms'));
if (!empty($preferredSonosRooms)) {
    $preferredSonosRoomsExists = true;
    // error_log("sonos_module backend: " . print_r ($preferredSonosRooms, true));
}


$lastfmApiKeyExists = false;
$lastfmApiKey= json_decode(getConfigValue('sonos_lastfmapikey'));
if (!empty($lastfmApiKey)) {
    $lastfmApiKeyExists = true;
}

$slideSpeed= json_decode(getConfigValue('sonos_slidespeed'));
if (empty($slideSpeed)) {
    $slideSpeed = 5;
}

$updateSpeed= json_decode(getConfigValue('sonos_updatespeed'));
if (empty($updateSpeed)) {
    $updateSpeed = 3;
}

$thumbnailStyle= json_decode(getConfigValue('sonos_thumbnailStyle'));
if (empty($thumbnailStyle) || $thumbnailStyle=='""') {
    $thumbnailStyle = 'sonosThumbnail';
}

$radioParser=array(); 
$radioParser=json_decode(str_replace('\\', '\\\\', getConfigValue('sonos_radio_parser')));
//error_log("sonos: radio parser config variable=" . print_r($radioParser,TRUE));
if (empty($radioParser)) {
    $radioParser=json_decode(str_replace('\\', '\\\\', "[{\"station\":\"SWR3\",\"field\":\"artist\",\"artist\":\"(.+)\\s\/\",\"title\":\".+\\s\/\\s(.+)\"},{\"station\":\"Bayern\\s3\",\"field\":\"artist\",\"title\":\".+:\\s(.+)\",\"artist\":\"(.+):.+\"},{\"station\":\"Bayern\\s1\",\"field\":\"artist\",\"title\":\".+:\\s(.+)\",\"artist\":\"(.+):.+\"},{\"station\":\"Rockland\\sRadio\",\"field\":\"artist\",\"title\":\".+-(.+)\",\"artist\":\"(.+)-.+\"}]"));
    
}


_('sonos_description');

?>

<p> <?php echo _('sonos_choose_rooms') ?><p>


<?php

//print_r( $preferredSonosRooms);

    ini_set("allow_url_fopen", 1);
    $getSonosRoomsUrl='http://127.0.0.1/modules/sonos/assets/getSonosControllersList.php';
   $ch = curl_init();
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_URL, $getSonosRoomsUrl);
$jsonRoomsData = curl_exec($ch);

curl_close($ch);

    $jsonDataObject = json_decode($jsonRoomsData, true);
    
    //print_r ($jsonDataObject);
    
    echo '<select name="preferred_sonos_rooms" id="preferred_sonos_rooms" multiple>';

    $foundPreferredRoom=false;
foreach ($jsonDataObject['rooms'] as $option) {
        
    foreach ($preferredSonosRooms as $preRoom) {
        if (strcmp($option['room'], $preRoom)==0) {
            $foundPreferredRoom=true;
            break;
        }
    }
    if (!$foundPreferredRoom) {
          echo '<option value="' . $option['room'] . '" >' . htmlentities($option['room']) . '</option>';
             
    } else {
        echo '<option value="' . $option['room'] . '" selected="selected">' . htmlentities($option['room']) . '</option>';
        $foundPreferredRoom=false;
    }
}
    echo '</select>';
    
?>

<script type="text/javascript">

if(typeof window.Tabulator == 'undefined'){
        document.write('<link rel="stylesheet" type="text/css" href="/modules/sonos/assets/tabulator-master/dist/css/tabulator_simple.css">');
        
        document.write('<script type="text/javascript" src="/modules/sonos/assets/tabulator-master/dist/js/tabulator.min.js"></'+'script>');
  }
</script>


<br>
<p><?php echo _('sonos_add_lastfmapikey') ?></p>
<input type="text" placeholder="<?php echo _('sonos_placeholder_lastfmapikey') ?>" id="lastfmApiKey" value="<?php echo $lastfmApiKey ?>"></input>


<br>
<p><?php echo _('sonos_set_slide_speed') ?></p>
<input type="number" min="2" max="100" step="1" placeholder="<?php echo _('sonos_placeholder_slideSpeed') ?>" id="slideSpeed" value="<?php echo $slideSpeed ?>"></input>

<br>
<p><?php echo _('sonos_set_update_speed') ?></p>
<input type="number" min="2" max="100" step="1" placeholder="<?php echo _('sonos_placeholder_updateSpeed') ?>" id="updateSpeed" value="<?php echo $updateSpeed ?>"></input>


<br>
<p><?php echo _('sonos_thumbnailStyle') ?></p>
<select id="thumbnailStyle">
  <option value="sonosThumbnail"><?php echo _('sonos_tn_blackwhite') ?></option>
  <option value="sonosThumbnailColor"><?php echo _('sonos_tn_color') ?></option>

</select>




<b><a id="showParserLink" onclick="$('#parser-table').show(); window.setTimeout(function(){$('#parser-table').tabulator('redraw', true);},800); $('#hideParserLink').show(); $('#addParserLink').show(); $('#helpParser').show(); $(this).hide();"><?php echo _('sonos_parser_show') ?></a></b>


<b><a id="hideParserLink" onclick="$('#parser-table').hide(); $('#helpParser').hide(); $('#showParserLink').show();  $(this).hide(); $('#addParserLink').hide(); " style='display: none'><?php echo _('sonos_parser_hide') ?></a></b>
<br>
<div id="helpParser" style='display: none'><p><?php echo _('sonos_radio_parser_define')?></p>
<br><?php echo _('sonos_parser_help')?>.
<br><br>
</div>
<div id="parser-table" style='display: none'></div>


&nbsp; &nbsp; &nbsp;
<a id="addParserLink" onclick="$('#parser-table').tabulator('addRow',emptyRow,false);" style='display: none'><?php echo _('sonos_parser_add_row')?></a>

    

<script>

    var fieldEditor = function(cell, onRendered, success, cancel, editorParams)
    {
        //cell - the cell component for the editable cell
        //onRendered - function to call when the editor has been rendered
        //success - function to call to pass the succesfully updated value to Tabulator
        //cancel - function to call to abort the edit and return to a normal cell
        //editorParams - editorParams object set in column defintion

        //create and style editor
        var editor = $("<select><option value='artist' select='selected'>Artist</option><option value='title'>Title</option><option value='current'>Station</option></select>");
        editor.css({
            "padding":"3px",
            "width":"100%",
            "box-sizing":"border-box",
        });

        //Set value of editor to the current value of the cell
        editor.val(cell.getValue());

        //set focus on the select box when the editor is selected (timeout allows for editor to be added to DOM)
        onRendered(function(){
            //console.log(editor.val());
            if( editor.val()===null)
            {
                editor.val('artist');
            }
            //console.log(editor.val());
            
          editor.children('input').each(function () 
            {
                if (this.value==editor.val()) 
                {
                    this.select();
                }
                
            });
          editor.focus();
        });

        //when the value has been set, trigger the cell to update
        editor.on("change blur", function(e){
            
            
                success(editor.val()); 
               
        });

        //return the editor element
        return editor;
    };

    
    var emptyValDeleteRow = function(cell, onRendered, success, cancel, editorParams)
    {
        //cell - the cell component for the editable cell
        //onRendered - function to call when the editor has been rendered
        //success - function to call to pass the succesfully updated value to Tabulator
        //cancel - function to call to abort the edit and return to a normal cell
        //editorParams - editorParams object set in column defintion

        //create and style editor
        var editor = $("<input type='text'></input>");
        
        editor.css({
               "padding":"3px",
               "width":"100%",
               "box-sizing":"border-box",
           });

        //Set value of editor to the current value of the cell
        editor.val(cell.getValue());

        //set focus on the select box when the editor is selected (timeout allows for editor to be added to DOM)
        onRendered(function(){
    
           
        });

        //when the value has been set, trigger the cell to update
        editor.on("change blur", function(e){
            
            if(editor.val()=="")
            {
                cell.getRow().delete();
            }
            else
            {
                success(editor.val());
            }
            
           
        });

        //return the editor element
        return editor;
    };
    



    var tableData = <?php echo json_encode($radioParser); ?>; 
    
    //console.log(tableData);
    
    var emptyRow = { "station": "Station", "field": "artist", "title": "(.+)\\s\\/", "artist": ".+\\s\\/\\s(.+)"};
    
$(document).on('ready', function() 
    {
        
        
        $("#parser-table").tabulator({
            height:"311px",
            layout:"fitColumns",
            columns:[
                        { title:"Station Name", field:"station", sorter:"string", editor:emptyValDeleteRow},
                        { title:"Field to Parse", field:"field", sorter:"string", editor:fieldEditor},
                        { title:"Artist Regex", field:"artist", sorter:"string", editor:true},
                        { title:"Title Regex", field:"title", sorter:"string", editor:true}
                
                    ],
            dataEdited:function(data)
                    {
                        //alert(JSON.stringify(data));
                        //alert(JSON.stringify(data));
                        $.post('/config/setConfigValueAjax.php', {'key' : 'sonos_radio_parser', 'value': JSON.stringify(data)})
                        .done(function() {
                                                $('#ok').show(30, function() {
                                                        $(this).hide('slow');
                                                });
                                            });
                        
                    },

            });
            $("#parser-table").tabulator("setData", tableData);
        }); //end document onload handler

</script>


<script>
    
    document.getElementById('thumbnailStyle').value="<?php echo $thumbnailStyle ?>";
    
    //alert (document.getElementById('thumbnailStyle').value) ;
    
    $('#lastfmApiKey').change(function() {
        var lastfmapikey = $("#lastfmApiKey").val();
        //alert(JSON.stringify(lastfmapikey));
        $.post('/config/setConfigValueAjax.php', {'key' : 'sonos_lastfmapikey', 'value': JSON.stringify(lastfmapikey)}).done(function() {
                                                    $('#ok').show(30, function() {
                                                            $(this).hide('slow');
                                                    });
                                                });
        });
    
        $('#thumbnailStyle').change(function() {
            var thumbnailStyle = $("#thumbnailStyle").val();
            //alert(JSON.stringify(thumbnailStyle));
            $.post('/config/setConfigValueAjax.php', {'key' : 'sonos_thumbnailStyle', 'value': JSON.stringify(thumbnailStyle)}).done(function() {
                                                        $('#ok').show(30, function() {
                                                                $(this).hide('slow');
                                                        });
                                                    });
            });    
        
        
        $('#slideSpeed').change(function() {
            var slideSpeed = $("#slideSpeed").val();
            //alert(JSON.stringify(slideSpeed));
            $.post('/config/setConfigValueAjax.php', {'key' : 'sonos_slidespeed', 'value': JSON.stringify(slideSpeed)}).done(function() {
                                                    $('#ok').show(30, function() {
                                                            $(this).hide('slow');
                                                    });
                                                });
            });
        
        $('#updateSpeed').change(function() {
            var updateSpeed = $("#updateSpeed").val();
            //alert(JSON.stringify(updateSpeed));
            $.post('/config/setConfigValueAjax.php', {'key' : 'sonos_updatespeed', 'value': JSON.stringify(updateSpeed)}).done(function() {
                                                    $('#ok').show(30, function() {
                                                            $(this).hide('slow');
                                                    });
                                                });
            });
        
    $('#preferred_sonos_rooms').change(function() {
        var newRooms = $("#preferred_sonos_rooms").val();
        //alert(JSON.stringify(newRooms));
        $.post('/config/setConfigValueAjax.php', {'key' : 'sonos_preferred_rooms', 'value': JSON.stringify(newRooms)}).done(function() {
                                                    $('#ok').show(30, function() {
                                                            $(this).hide('slow');
                                                    });
                                                });
        });
</script>

