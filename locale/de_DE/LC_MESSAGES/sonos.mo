��          �      l      �     �     �               $     6     K     ]     o     �     �     �     �     �          "     7     C     W     f    t  K  �     �  	   �  O   �  x   @     �  �  �     i	     �	     �	     �	     �	     �	  {   
  V   ~
  &   �
     �
                                  	                         
                                                      sonos_add_lastfmapikey sonos_album sonos_artist sonos_choose_rooms sonos_description sonos_parser_add_row sonos_parser_help sonos_parser_hide sonos_parser_show sonos_placeholder_lastfmapikey sonos_placeholder_slideSpeed sonos_placeholder_updateSpeed sonos_radio_parser_define sonos_set_slide_speed sonos_set_update_speed sonos_thumbnailStyle sonos_title sonos_tn_blackwhite sonos_tn_color sonos_up_next Project-Id-Version: mirr.OS Sonos Module V1.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-03-14 11:54+0100
PO-Revision-Date: 2018-03-16 16:31+0100
Last-Translator: Tobias Grasse <tg@glancr.de>
Language-Team: German (Germany)
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Loco-Source-Locale: de_DE
X-Generator: Poedit 2.0.6
X-Poedit-SourceCharset: UTF-8
X-Loco-Parser: loco_parse_po
X-Poedit-Basepath: ../../..
X-Poedit-SearchPath-0: .
 Um auch bei einigen Radiostationen Album-Covers darzustellen, ist ein LastFM API Key erforderlich. Hinweis: Nicht alle Radiostationen/Streams senden den Künstler und Titel korrekt getrennt, deshalb kann dann kein Album-Cover dargestellt werden… es sei denn, im Advanced Radio Parser weiter unten werden Einstellungen vorgenommen Album Künstler Wähle ein oder mehrere Räume aus, die auf dem Spiegel angezeigt werden sollen Dieses Modul zeigt dir den aktuellen und den nächsten Titel sowie den Fortschritt Deiner ausgewählten Sonos Systeme an Station hinzuf&uuml;gen Wer kein Regex kann, hat hier die M&ouml;glichkeit, dies zu lernen und anzuwenden. Die „Station“ beinhaltet das Regex, welches die Station identifiziert. Das „Field“ benennt das Feld, welches die zusammengesetzten Infos bgzl. K&uuml;nstler und Titel beinhaltet. Die Regexes f&uuml;r diese beiden Felder folgen danach, jedes muss eine Regex-Gruppe beinhalten, welche die jeweilige Info identifiziert. Verberge Advanced Radio Parser Zeige Advanced Radio Parser LastFM API Key Anzeigedauer in Sekunden Pause zwischen Aktualisierungen Advanced Radio Parser Sind mehrere Räume ausgewählt, werden diese als Diashow dargestellt. Setze hier die Anzeigedauer eines Raumes in Sekunden Lege die Dauer eines Aktualisierungs-Intervalles in Sekunden fest. Standardwert ist 3. W&auml;hle den Stil f&uuml;r die Cover Sonos Schwarz/Wei&szlig; Farbig Als nächstes 