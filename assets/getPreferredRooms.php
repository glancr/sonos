<?php
include('../../../config/glancrConfig.php');

$preferredRooms= getConfigValue('sonos_preferred_rooms');
// wenn der parameter noch nicht gesetzt wurde
if($preferredRooms == 'GLANCR_DEFAULT') {
	$preferredRooms = '';
}

echo json_encode($preferredRooms);

?>
